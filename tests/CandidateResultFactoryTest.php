<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-vote-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Vote\CandidateResultFactory;
use PHPUnit\Framework\TestCase;

/**
 * CandidateResultFactoryTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Vote\CandidateResultFactory
 *
 * @internal
 *
 * @small
 */
class CandidateResultFactoryTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var CandidateResultFactory
	 */
	protected CandidateResultFactory $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new CandidateResultFactory();
	}
	
}
