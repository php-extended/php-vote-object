<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-vote-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Vote\CitizenInterface;
use PhpExtended\Vote\InvalidVoteException;
use PhpExtended\Vote\VoteInterface;
use PHPUnit\Framework\TestCase;

/**
 * InvalidVoteExceptionTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Vote\InvalidVoteException
 *
 * @internal
 *
 * @small
 */
class InvalidVoteExceptionTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var InvalidVoteException
	 */
	protected InvalidVoteException $_object;
	
	public function testToString() : void
	{
		$this->assertStringContainsString(\get_class($this->_object), $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new InvalidVoteException(
			$this->getMockForAbstractClass(CitizenInterface::class),
			$this->getMockForAbstractClass(VoteInterface::class),
		);
	}
	
}
