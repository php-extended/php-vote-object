<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-vote-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Vote\ElectionRunner;
use PHPUnit\Framework\TestCase;

/**
 * ElectionRunnerTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Vote\ElectionRunner
 *
 * @internal
 *
 * @small
 */
class ElectionRunnerTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ElectionRunner
	 */
	protected ElectionRunner $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ElectionRunner();
	}
	
}
