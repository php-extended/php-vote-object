<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-vote-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Vote\UniqueFloatCandidate;
use PHPUnit\Framework\TestCase;

/**
 * UniqueFloatCandidateTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Vote\UniqueFloatCandidate
 * @internal
 * @small
 */
class UniqueFloatCandidateTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var UniqueFloatCandidate
	 */
	protected UniqueFloatCandidate $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetValue() : void
	{
		$this->assertEquals(1.2, $this->_object->getValue());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new UniqueFloatCandidate('id', 1.2);
	}
	
}
