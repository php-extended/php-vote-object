<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-vote-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Vote\FirstPastThePostVotingMethod;
use PHPUnit\Framework\TestCase;

/**
 * FirstPastThePostVotingMethodTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Vote\FirstPastThePostVotingMethod
 *
 * @internal
 *
 * @small
 */
class FirstPastThePostVotingMethodTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var FirstPastThePostVotingMethod
	 */
	protected FirstPastThePostVotingMethod $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object), $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new FirstPastThePostVotingMethod();
	}
	
}
