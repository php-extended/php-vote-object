<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-vote-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Vote\ProposedCandidate;
use PhpExtended\Vote\UniqueBooleanCandidate;
use PhpExtended\Vote\UniqueBooleanEqualsCitizen;
use PHPUnit\Framework\TestCase;

/**
 * ProposedCandidateTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Vote\ProposedCandidate
 *
 * @internal
 *
 * @small
 */
class ProposedCandidateTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ProposedCandidate
	 */
	protected ProposedCandidate $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetCitizen() : void
	{
		$this->assertEquals(new UniqueBooleanEqualsCitizen('id'), $this->_object->getCitizen());
	}
	
	public function testGetCandidate() : void
	{
		$this->assertEquals(new UniqueBooleanCandidate('id', true), $this->_object->getCandidate());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ProposedCandidate(new UniqueBooleanEqualsCitizen('id'), new UniqueBooleanCandidate('id', true));
	}
	
}
