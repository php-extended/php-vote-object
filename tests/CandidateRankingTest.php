<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-vote-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Score\BooleanScore;
use PhpExtended\Vote\CandidateRanking;
use PHPUnit\Framework\TestCase;

/**
 * CandidateRankingTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Vote\CandidateRanking
 *
 * @internal
 *
 * @small
 */
class CandidateRankingTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var CandidateRanking
	 */
	protected CandidateRanking $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@[id]', $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new CandidateRanking('id', new BooleanScore(true));
	}
	
}
