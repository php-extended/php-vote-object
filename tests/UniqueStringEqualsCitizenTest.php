<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-vote-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Vote\UniqueStringEqualsCitizen;
use PHPUnit\Framework\TestCase;

/**
 * UniqueStringEqualsCitizenTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Vote\UniqueStringEqualsCitizen
 *
 * @internal
 *
 * @small
 */
class UniqueStringEqualsCitizenTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var UniqueStringEqualsCitizen
	 */
	protected UniqueStringEqualsCitizen $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@[id]', $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new UniqueStringEqualsCitizen('id');
	}
	
}
