<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-vote-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Vote\UniqueBooleanEqualsCitizen;
use PHPUnit\Framework\TestCase;

/**
 * UniqueBooleanEqualsCitizenTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Vote\UniqueBooleanEqualsCitizen
 *
 * @internal
 *
 * @small
 */
class UniqueBooleanEqualsCitizenTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var UniqueBooleanEqualsCitizen
	 */
	protected UniqueBooleanEqualsCitizen $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@[id]', $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new UniqueBooleanEqualsCitizen('id');
	}
	
}
