<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-vote-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Vote\CandidateInterface;
use PhpExtended\Vote\CitizenInterface;
use PhpExtended\Vote\InvalidCandidateException;
use PHPUnit\Framework\TestCase;

/**
 * InvalidCandidateExceptionTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Vote\InvalidCandidateException
 *
 * @internal
 *
 * @small
 */
class InvalidCandidateExceptionTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var InvalidCandidateException
	 */
	protected InvalidCandidateException $_object;
	
	public function testToString() : void
	{
		$this->assertStringContainsString(\get_class($this->_object), $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new InvalidCandidateException(
			$this->getMockForAbstractClass(CitizenInterface::class),
			$this->getMockForAbstractClass(CandidateInterface::class),
		);
	}
	
}
