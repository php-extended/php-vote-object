<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-vote-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Score\BooleanScore;
use PhpExtended\Vote\Vote;
use PHPUnit\Framework\TestCase;

/**
 * VoteTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Vote\Vote
 *
 * @internal
 *
 * @small
 */
class VoteTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var Vote
	 */
	protected Vote $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@[id]', $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new Vote('id', new BooleanScore(true));
	}
	
}
