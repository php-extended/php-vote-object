<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-vote-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Score\BooleanScore;
use PhpExtended\Vote\CandidateResult;
use PhpExtended\Vote\UniqueBooleanCandidate;
use PHPUnit\Framework\TestCase;

/**
 * CandidateResultTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Vote\CandidateResult
 *
 * @internal
 *
 * @small
 */
class CandidateResultTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var CandidateResult
	 */
	protected CandidateResult $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@[id]', $this->_object->__toString());
	}
	
	public function testGetId() : void
	{
		$this->assertEquals('id', $this->_object->getId());
	}
	
	public function testGetCandidate() : void
	{
		$this->assertEquals(new UniqueBooleanCandidate('id', true), $this->_object->getCandidate());
	}
	
	public function testGetPoints() : void
	{
		$this->assertEquals(0, $this->_object->getPoints());
	}
	
	public function testGetScore() : void
	{
		$this->assertEquals(new BooleanScore(true), $this->_object->getScore());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new CandidateResult('id', new UniqueBooleanCandidate('id', true), 0, new BooleanScore(true));
	}
	
}
