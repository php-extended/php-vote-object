<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-vote-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Vote\UniqueBooleanCandidate;
use PhpExtended\Vote\UniqueCandidateFactory;
use PhpExtended\Vote\UniqueFloatCandidate;
use PhpExtended\Vote\UniqueIntegerCandidate;
use PhpExtended\Vote\UniqueStringCandidate;
use PHPUnit\Framework\TestCase;

/**
 * UniqueCandidateFactoryTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Vote\UniqueCandidateFactory
 * @internal
 * @small
 */
class UniqueCandidateFactoryTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var UniqueCandidateFactory
	 */
	protected UniqueCandidateFactory $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testCreateBooleanCandidate() : void
	{
		$this->assertInstanceOf(UniqueBooleanCandidate::class, $this->_object->createBooleanCandidate('id', true));
	}
	
	public function testCreateIntegerCandidate() : void
	{
		$this->assertInstanceOf(UniqueIntegerCandidate::class, $this->_object->createIntegerCandidate('id', 2));
	}
	
	public function testCreateFloatCandidate() : void
	{
		$this->assertInstanceOf(UniqueFloatCandidate::class, $this->_object->createFloatCandidate('id', 1.2));
	}
	
	public function testCreateStringCandidate() : void
	{
		$this->assertInstanceOf(UniqueStringCandidate::class, $this->_object->createStringCandidate('id', 'str'));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new UniqueCandidateFactory();
	}
	
}
