<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-vote-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Vote;

/**
 * Election class file.
 * 
 * This class is a simple implementation of the election interface that stores
 * all the objects in-memory.
 * 
 * @author Anastaszor
 * @template T of boolean|integer|float|string
 * @implements ElectionInterface<T>
 */
class Election implements ElectionInterface
{
	
	/**
	 * The quantity of votes created by this election.
	 * 
	 * @var integer
	 */
	protected int $_votesCreated = 0;
	
	/**
	 * The id of this election.
	 * 
	 * @var string
	 */
	protected string $_id;
	
	/**
	 * The candidates the election runs on.
	 * 
	 * @var array<string, ProposedCandidate<T>>
	 */
	protected array $_candidates = [];
	
	/**
	 * The votes the citizen makes.
	 * 
	 * @var array<string, ProposedVote<T>>
	 */
	protected array $_votes = [];
	
	/**
	 * The biases before the results.
	 * 
	 * @var array<integer, BiasInterface<T>>
	 */
	protected array $_biases = [];
	
	/**
	 * Builds a new Election with the given id.
	 * 
	 * @param string $ident
	 */
	public function __construct(string $ident)
	{
		$this->_id = $ident;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@['.$this->_id.']';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Vote\ElectionInterface::getId()
	 */
	public function getId() : string
	{
		return $this->_id;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Vote\ElectionInterface::registerCandidate()
	 */
	public function registerCandidate(CitizenInterface $citizen, CandidateInterface $candidate) : string
	{
		$this->_candidates[$candidate->getId()] = new ProposedCandidate($citizen, $candidate);
		
		return $candidate->getId();
	}
	
	/**
	 * Gets the candidates only.
	 * 
	 * @return array<integer, CandidateInterface<T>>
	 */
	public function getCandidates() : array
	{
		$candidates = [];
		
		foreach($this->_candidates as $candidateProposal)
		{
			$candidates[] = $candidateProposal->getCandidate();
		}
		
		return $candidates;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Vote\ElectionInterface::registerVote()
	 * @throws InvalidVoteException
	 */
	public function registerVote(CitizenInterface $citizen, VoteInterface $vote) : string
	{
		foreach($vote->getCandidateRanking() as $candidateRanking)
		{
			foreach($candidateRanking->getCandidates() as $candidateProposal)
			{
				if(!isset($this->_candidates[$candidateProposal->getId()]))
				{
					throw new InvalidVoteException($citizen, $vote, \strtr('Candidate {h} not found', ['{h}' => $candidateProposal->getId()]));
				}
			}
		}
		
		if(isset($this->_votes[$citizen->getId()]))
		{
			throw new InvalidVoteException($citizen, $vote, \strtr('Citizen {c} has already voted.', ['{c}' => $citizen->getId()]));
		}
		
		$this->_votes[$citizen->getId()] = new ProposedVote($citizen, $vote);
		
		return $vote->getId();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Vote\ElectionInterface::registerBias()
	 */
	public function registerBias(BiasInterface $bias) : bool
	{
		$this->_biases[] = $bias;
		
		return true;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Vote\ElectionInterface::getResult()
	 * @throws UnsolvableSituationThrowable
	 */
	public function getResult(VotingMethodInterface $votingMethod) : ElectionResultInterface
	{
		$finalVotes = [];
		
		foreach($this->_votes as $voteProposal)
		{
			$vote = $voteProposal->getVote();
			
			foreach($this->_biases as $bias)
			{
				$vote = $bias->applyTo($voteProposal->getCitizen(), $vote);
			}
			
			$finalVotes[] = $vote;
		}
		
		return $votingMethod->resolve($this, $this->getCandidates(), $finalVotes);
	}
	
}
