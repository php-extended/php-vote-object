<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-vote-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Vote;

/**
 * ElectionResult class file.
 * 
 * This class is a simple implementation of the ElectionResultInterface.
 * 
 * @author Anastaszor
 * @template T of boolean|integer|float|string
 * @implements ElectionResultInterface<T>
 */
class ElectionResult implements ElectionResultInterface
{
	
	/**
	 * The identifier of this result.
	 * 
	 * @var string
	 */
	protected string $_id;
	
	/**
	 * The rankings for this result.
	 * 
	 * @var array<integer, CandidateResultInterface<T>>
	 */
	protected array $_rankings = [];
	
	/**
	 * Builds a new ElectionResult with the given id and rankings.
	 * 
	 * @param string $ident
	 * @param array<integer, CandidateResultInterface<T>> $rankings
	 */
	public function __construct(string $ident, array $rankings = [])
	{
		$this->_id = $ident;
		
		foreach($rankings as $ranking)
		{
			$this->_rankings[] = $ranking;
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@['.$this->_id.']';
	}
	
	/**
	 * Adds a ranking to the existing ones.
	 * 
	 * @param CandidateResultInterface<T> $ranking
	 */
	public function addRanking(CandidateResultInterface $ranking) : void
	{
		$this->_rankings[] = $ranking;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Vote\ElectionResultInterface::getId()
	 */
	public function getId() : string
	{
		return $this->_id;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Vote\ElectionResultInterface::getRankings()
	 */
	public function getRankings() : array
	{
		return \array_values($this->_rankings);
	}
	
}
