<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-vote-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Vote;

use Stringable;

/**
 * ProposedVote class file.
 * 
 * This class represents a vote and the citizen that proposed it.
 * 
 * @author Anastaszor
 * @template T of boolean|integer|float|string
 */
class ProposedVote implements Stringable
{
	
	/**
	 * The citizen that proposed the vote.
	 * 
	 * @var CitizenInterface<T>
	 */
	protected CitizenInterface $_citizen;
	
	/**
	 * The vote that was proposed by the citizen.
	 * 
	 * @var VoteInterface<T>
	 */
	protected VoteInterface $_vote;
	
	/**
	 * Builds a new ProposedVote with the given citizen and vote.
	 * 
	 * @param CitizenInterface<T> $citizen
	 * @param VoteInterface<T> $vote
	 */
	public function __construct(CitizenInterface $citizen, VoteInterface $vote)
	{
		$this->_citizen = $citizen;
		$this->_vote = $vote;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Gets the citizen that proposed the vote.
	 * 
	 * @return CitizenInterface<T>
	 */
	public function getCitizen() : CitizenInterface
	{
		return $this->_citizen;
	}
	
	/**
	 * Gets the vote that was proposed by the citizen.
	 * 
	 * @return VoteInterface<T>
	 */
	public function getVote() : VoteInterface
	{
		return $this->_vote;
	}
	
}
