<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-vote-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Vote;

use Stringable;

/**
 * ProposedCandidate class file.
 * 
 * This class represents a candidate and the citizen that proposed it.
 * 
 * @author Anastaszor
 * @template T of boolean|integer|float|string
 */
class ProposedCandidate implements Stringable
{
	
	/**
	 * The citizen that proposed the candidate.
	 * 
	 * @var CitizenInterface<T>
	 */
	protected CitizenInterface $_citizen;
	
	/**
	 * The candidate that was proposed by the citizen.
	 * 
	 * @var CandidateInterface<T>
	 */
	protected CandidateInterface $_candidate;
	
	/**
	 * Builds a new ProposedCandidate with the given citizen and candidate.
	 * 
	 * @param CitizenInterface<T> $citizen
	 * @param CandidateInterface<T> $candidate
	 */
	public function __construct(CitizenInterface $citizen, CandidateInterface $candidate)
	{
		$this->_citizen = $citizen;
		$this->_candidate = $candidate;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Gets the citizen that proposed the candidate.
	 * 
	 * @return CitizenInterface<T>
	 */
	public function getCitizen() : CitizenInterface
	{
		return $this->_citizen;
	}
	
	/**
	 * Gets the candidate that was proposed by the citizen.
	 * 
	 * @return CandidateInterface<T>
	 */
	public function getCandidate() : CandidateInterface
	{
		return $this->_candidate;
	}
	
}
