<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-vote-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Vote;

/**
 * StringCandidate class file.
 * 
 * A Candidate that holds string values.
 * 
 * @author Anastaszor
 * @extends AbstractCandidate<string>
 */
class UniqueStringCandidate extends AbstractCandidate
{
	
	/**
	 * The value of this candidate.
	 * 
	 * @var ?string
	 */
	protected ?string $_value;
	
	/**
	 * Builds a new Candidate with its value.
	 * 
	 * @param string $id
	 * @param ?string $value
	 */
	public function __construct(string $id, ?string $value)
	{
		parent::__construct($id);
		$this->_value = $value;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Vote\CandidateInterface::getValue()
	 */
	public function getValue() : ?string
	{
		return $this->_value;
	}
	
}
