<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-vote-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Vote;

/**
 * ElectionRunner class file.
 * 
 * This class is a simple implementation of the ElectionRunnerInterface. This
 * class follows the workflow :
 * 
 * 1 - registers all the candidates given by the citizens
 * 2 - registers all the biases
 * 3 - let all citizens to review all the candidates
 * 4 - registers all the votes from the citizens
 * 5 - delegate to the method the calculus of the result
 * 
 * @author Anastaszor
 */
class ElectionRunner implements ElectionRunnerInterface
{
	
	/**
	 * The current count of elections.
	 * 
	 * @var integer
	 */
	protected static int $_electionCount = 0;
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Vote\ElectionRunnerInterface::runElection()
	 * @template T of boolean|integer|float|string
	 * @throws InvalidCandidateThrowable
	 * @throws UnsolvableSituationThrowable
	 */
	public function runElection(VotingMethodInterface $votingMethod, array $biases, array $citizens) : ElectionResultInterface
	{
		/** @var Election<T> $election */
		$election = new Election('ELEC'.((string) (static::$_electionCount++)));
		
		foreach($citizens as $citizen)
		{
			/** @phpstan-ignore-next-line */
			foreach($citizen->proposeCandidates($election) as $candidate)
			{
				$election->registerCandidate($citizen, $candidate);
			}
		}
		
		foreach($biases as $bias)
		{
			$election->registerBias($bias);
		}
		
		foreach($citizens as $citizen)
		{
			foreach($election->getCandidates() as $candidate)
			{
				/** @phpstan-ignore-next-line */
				$citizen->reviewCandidate($election, $candidate);
			}
		}
		
		foreach($citizens as $citizen)
		{
			/** @phpstan-ignore-next-line */
			$election->registerVote($citizen, $citizen->vote($election));
		}
		
		/** @phpstan-ignore-next-line */
		return $election->getResult($votingMethod);
	}
	
}
