<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-vote-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Vote;

/**
 * CandidateFactory class file.
 * 
 * This class is a simple implementation of the CandidateFactoryInterface.
 * 
 * @author Anastaszor
 */
class UniqueCandidateFactory implements CandidateFactoryInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Vote\CandidateFactoryInterface::createBooleanCandidate()
	 */
	public function createBooleanCandidate(string $identifier, ?bool $value) : CandidateInterface
	{
		return new UniqueBooleanCandidate($identifier, $value);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Vote\CandidateFactoryInterface::createIntegerCandidate()
	 */
	public function createIntegerCandidate(string $identifier, ?int $value) : CandidateInterface
	{
		return new UniqueIntegerCandidate($identifier, $value);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Vote\CandidateFactoryInterface::createFloatCandidate()
	 */
	public function createFloatCandidate(string $identifier, ?float $value) : CandidateInterface
	{
		return new UniqueFloatCandidate($identifier, $value);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Vote\CandidateFactoryInterface::createStringCandidate()
	 */
	public function createStringCandidate(string $identifier, ?string $value) : CandidateInterface
	{
		return new UniqueStringCandidate($identifier, $value);
	}
	
}
