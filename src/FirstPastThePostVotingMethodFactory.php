<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-vote-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Vote;

/**
 * FirstPastThePostVotingMethodFactory class file.
 * 
 * This class is a factory for first-past-the-post voting methods.
 * 
 * @author Anastaszor
 */
class FirstPastThePostVotingMethodFactory implements VotingMethodFactoryInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Vote\VotingMethodFactoryInterface::createVotingMethod()
	 */
	public function createVotingMethod() : VotingMethodInterface
	{
		return new FirstPastThePostVotingMethod();
	}
	
}
