<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-vote-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Vote;

use PhpExtended\Score\ScoreInterface;

/**
 * Vote class file.
 * 
 * This class is a simple implementation of the VoteInterface.
 * 
 * @author Anastaszor
 * @template T of boolean|integer|float|string
 * @implements VoteInterface<T>
 */
class Vote implements VoteInterface
{
	
	/**
	 * The identifier of this vote.
	 * 
	 * @var string
	 */
	protected string $_id;
	
	/**
	 * The score of this vote given by the citizen.
	 *
	 * @var ScoreInterface
	 */
	protected ScoreInterface $_score;
	
	/**
	 * The rankings of the candidates.
	 * 
	 * @var array<integer, CandidateRankingInterface<T>>
	 */
	protected array $_rankings = [];
	
	/**
	 * Builds a new Vote with the given Candidate Rankings.
	 * 
	 * @param string $ident
	 * @param ScoreInterface $score
	 * @param array<integer, CandidateRankingInterface<T>> $rankings
	 */
	public function __construct(string $ident, ScoreInterface $score, array $rankings = [])
	{
		$this->_id = $ident;
		$this->_score = $score;
		
		foreach($rankings as $ranking)
		{
			$this->_rankings[] = $ranking;
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@['.$this->_id.']';
	}
	
	/**
	 * Adds a candidate ranking for this vote.
	 * 
	 * @param CandidateRankingInterface<T> $ranking
	 */
	public function addCandidateRanking(CandidateRankingInterface $ranking) : void
	{
		$this->_rankings[] = $ranking;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Vote\VoteInterface::getId()
	 */
	public function getId() : string
	{
		return $this->_id;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Vote\VoteInterface::getCandidateRanking()
	 */
	public function getCandidateRanking() : array
	{
		return \array_values($this->_rankings);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Vote\VoteInterface::getOverallScore()
	 */
	public function getOverallScore() : ScoreInterface
	{
		return $this->_score;
	}
	
}
