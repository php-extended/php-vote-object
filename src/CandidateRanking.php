<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-vote-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Vote;

use PhpExtended\Score\ScoreInterface;

/**
 * CandidateRanking class file.
 * 
 * This class is a simple implementation of the CandidateRankingInterface.
 * 
 * @author Anastaszor
 * @template T of boolean|integer|float|string
 * @implements CandidateRankingInterface<T>
 */
class CandidateRanking implements CandidateRankingInterface
{
	
	/**
	 * The id of this ranking.
	 * 
	 * @var string
	 */
	protected string $_id;
	
	/**
	 * The score of this ranking.
	 *
	 * @var ScoreInterface
	 */
	protected ScoreInterface $_score;
	
	/**
	 * The candidates for this ranking.
	 * 
	 * @var array<integer, CandidateInterface<T>>
	 */
	protected array $_candidates = [];
	
	/**
	 * Builds a new CandidateRanking with the given candidates and overall score.
	 * 
	 * @param string $ident
	 * @param ScoreInterface $score
	 * @param array<integer, CandidateInterface<T>> $candidates
	 */
	public function __construct(string $ident, ScoreInterface $score, array $candidates = [])
	{
		$this->_id = $ident;
		$this->_score = $score;
		
		foreach($candidates as $candidate)
		{
			$this->_candidates[] = $candidate;
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@['.$this->_id.']';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Vote\CandidateRankingInterface::getId()
	 */
	public function getId() : string
	{
		return $this->_id;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Vote\CandidateRankingInterface::getCandidates()
	 */
	public function getCandidates() : array
	{
		return \array_values($this->_candidates);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Vote\CandidateRankingInterface::getScore()
	 */
	public function getScore() : ScoreInterface
	{
		return $this->_score;
	}
	
}
