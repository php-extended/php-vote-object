<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-vote-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Vote;

/**
 * AbstractCandidate class file.
 * 
 * A generic candidate that handles all the generic stuff for a candidate.
 * 
 * @author Anastaszor
 * @template T of boolean|integer|float|string
 * @implements CandidateInterface<T>
 */
abstract class AbstractCandidate implements CandidateInterface
{
	
	/**
	 * The id of this candidate.
	 * 
	 * @var string
	 */
	protected string $_id;
	
	/**
	 * Builds a new AbstractCandidate with its id.
	 * 
	 * @param string $id
	 */
	public function __construct(string $id)
	{
		$this->_id = $id;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Vote\CandidateInterface::getId()
	 */
	public function getId() : string
	{
		return $this->_id;
	}
	
}
