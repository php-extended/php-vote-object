<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-vote-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Vote;

use PhpExtended\Score\BooleanScore;
use PhpExtended\Score\StringEqualsScore;

/**
 * UniqueStringEqualsCitizen class file.
 * 
 * This class represents a citizen that votes only for candidates that have
 * an unique argument which is a string value, and if that value is the
 * string value they care for.
 * 
 * This citizen votes only with boolean scores, meaning the score is 100% if
 * the candidate carries an unique argument string value and that value matches
 * with this citizen's value, and 0% in any other case.
 * 
 * @author Anastaszor
 * @implements CitizenInterface<string>
 */
class UniqueStringEqualsCitizen implements CitizenInterface
{
	
	/**
	 * The identifier of this citizen.
	 * 
	 * @var string
	 */
	protected string $_id;
	
	/**
	 * The string value that is carried with this citizen.
	 * 
	 * @var ?string
	 */
	protected ?string $_value = null;
	
	/**
	 * Builds a new StringCitizen with the given id and value.
	 * 
	 * @param string $ident
	 * @param ?string $value
	 */
	public function __construct(string $ident, ?string $value = null)
	{
		$this->_id = $ident;
		$this->_value = $value;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@['.$this->_id.']';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Vote\CitizenInterface::getId()
	 */
	public function getId() : string
	{
		return $this->_id;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Vote\CitizenInterface::proposeCandidates()
	 */
	public function proposeCandidates(ElectionInterface $election) : array
	{
		return [new UniqueStringCandidate($this->_id, $this->_value)];
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Vote\CitizenInterface::reviewCandidate()
	 */
	public function reviewCandidate(ElectionInterface $election, CandidateInterface $candidate) : bool
	{
		return true;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Vote\CitizenInterface::vote()
	 */
	public function vote(ElectionInterface $election) : VoteInterface
	{
		/** @var Vote<string> $vote */
		$vote = new Vote($election->getId().'_'.$this->_id, new BooleanScore(true));
		
		foreach($election->getCandidates() as $candidate)
		{
			$ident = $election->getId().'_'.$this->_id.'_'.$candidate->getId();
			$score = new StringEqualsScore($this->_value, $candidate->getValue());
			$ranking = new CandidateRanking($ident, $score, [$candidate]);
			$vote->addCandidateRanking($ranking);
		}
		
		return $vote;
	}
	
}
