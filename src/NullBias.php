<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-vote-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Vote;

/**
 * NullBias class file.
 * 
 * This class is a bias that does nothing to the votes.
 * 
 * @author Anastaszor
 * @template T of boolean|integer|float|string
 * @implements BiasInterface<T>
 */
class NullBias implements BiasInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Vote\BiasInterface::applyTo()
	 */
	public function applyTo(CitizenInterface $citizen, VoteInterface $vote) : VoteInterface
	{
		return $vote;
	}
	
}
