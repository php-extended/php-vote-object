<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-vote-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Vote;

/**
 * NullBiasFactory class file.
 * 
 * This class creates biases that does nothing.
 * 
 * @author Anastaszor
 */
class NullBiasFactory implements BiasFactoryInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Vote\BiasFactoryInterface::createBooleanBias()
	 * @psalm-suppress MoreSpecificReturnType
	 */
	public function createBooleanBias() : BiasInterface
	{
		/** @phpstan-ignore-next-line */ /** @psalm-suppress LessSpecificReturnStatement */
		return new NullBias();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Vote\BiasFactoryInterface::createIntegerBias()
	 * @psalm-suppress MoreSpecificReturnType
	 */
	public function createIntegerBias() : BiasInterface
	{
		/** @phpstan-ignore-next-line */ /** @psalm-suppress LessSpecificReturnStatement */
		return new NullBias();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Vote\BiasFactoryInterface::createFloatBias()
	 * @psalm-suppress MoreSpecificReturnType
	 */
	public function createFloatBias() : BiasInterface
	{
		/** @phpstan-ignore-next-line */ /** @psalm-suppress LessSpecificReturnStatement */
		return new NullBias();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Vote\BiasFactoryInterface::createStringBias()
	 * @psalm-suppress MoreSpecificReturnType
	 */
	public function createStringBias() : BiasInterface
	{
		/** @phpstan-ignore-next-line */ /** @psalm-suppress LessSpecificReturnStatement */
		return new NullBias();
	}
	
}
