<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-vote-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Vote;

/**
 * FloatCandidate class file.
 * 
 * A Candidate that holds float values.
 * 
 * @author Anastaszor
 * @extends AbstractCandidate<float>
 */
class UniqueFloatCandidate extends AbstractCandidate
{
	
	/**
	 * The value of this candidate.
	 * 
	 * @var ?float
	 */
	protected ?float $_value;
	
	/**
	 * Builds a new Candidate with its value.
	 * 
	 * @param string $id
	 * @param ?float $value
	 */
	public function __construct(string $id, ?float $value)
	{
		parent::__construct($id);
		$this->_value = $value;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Vote\CandidateInterface::getValue()
	 */
	public function getValue() : ?float
	{
		return $this->_value;
	}
	
}
