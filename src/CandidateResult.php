<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-vote-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Vote;

use PhpExtended\Score\ScoreInterface;

/**
 * CandidateResult class file.
 * 
 * This class is a simple implementation of the CandidateResultInterface.
 * 
 * @author Anastaszor
 * @template T of boolean|integer|float|string
 * @implements CandidateResultInterface<T>
 */
class CandidateResult implements CandidateResultInterface
{
	
	/**
	 * The id of this result.
	 * 
	 * @var string
	 */
	protected string $_id;
	
	/**
	 * The candidate for this result.
	 * 
	 * @var CandidateInterface<T>
	 */
	protected CandidateInterface $_candidate;
	
	/**
	 * The number of points of this candidate.
	 * 
	 * @var integer
	 */
	protected int $_points = 0;
	
	/**
	 * The score of this candidate.
	 * 
	 * @var ScoreInterface
	 */
	protected ScoreInterface $_score;
	
	/**
	 * Builds a new CandidateResult with the given candidate, its points, and 
	 * its score.
	 * 
	 * @param string $ident
	 * @param CandidateInterface<T> $candidate
	 * @param integer $points
	 * @param ScoreInterface $score
	 */
	public function __construct(string $ident, CandidateInterface $candidate, int $points, ScoreInterface $score)
	{
		$this->_id = $ident;
		$this->_candidate = $candidate;
		$this->_points = $points;
		$this->_score = $score;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@['.$this->_id.']';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Vote\CandidateResultInterface::getId()
	 */
	public function getId() : string
	{
		return $this->_id;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Vote\CandidateResultInterface::getCandidate()
	 */
	public function getCandidate() : CandidateInterface
	{
		return $this->_candidate;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Vote\CandidateResultInterface::getPoints()
	 */
	public function getPoints() : int
	{
		return $this->_points;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Vote\CandidateResultInterface::getScore()
	 */
	public function getScore() : ScoreInterface
	{
		return $this->_score;
	}
	
}
