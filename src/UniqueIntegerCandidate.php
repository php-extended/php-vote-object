<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-vote-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Vote;

/**
 * IntegerCandidate class file.
 * 
 * A candidate that holds integer values.
 * 
 * @author Anastaszor
 * @extends AbstractCandidate<integer>
 */
class UniqueIntegerCandidate extends AbstractCandidate
{
	
	/**
	 * The value of this candidate.
	 * 
	 * @var ?integer
	 */
	protected ?int $_value;
	
	/**
	 * Builds a new Candidate with its value.
	 * 
	 * @param string $id
	 * @param ?integer $value
	 */
	public function __construct(string $id, ?int $value)
	{
		parent::__construct($id);
		$this->_value = $value;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Vote\CandidateInterface::getValue()
	 */
	public function getValue() : ?int
	{
		return $this->_value;
	}
	
}
