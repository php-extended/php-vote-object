<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-vote-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Vote;

use PhpExtended\Score\IntegerScore;

/**
 * FirstPastThePostVotingMethod class file.
 * 
 * This class represents the first-past-the-post voting method. This method is
 * a method from plurality voting that states that the candidate that receives
 * the most votes wins. The candidates are ranked by their respective number
 * of votes, in decreasing order.
 * 
 * @author Anastaszor
 * @see https://en.wikipedia.org/wiki/First-past-the-post_voting
 * @todo use only iterators for large vote pools
 */
class FirstPastThePostVotingMethod implements VotingMethodInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Vote\VotingMethodInterface::resolve()
	 * @phpstan-template T of boolean|integer|float|string
	 */
	public function resolve(ElectionInterface $election, array $candidates, array $votes) : ElectionResultInterface
	{
		// /!\ Memory
		/** @var array<integer, FirstPastThePostVoteCount<T>> $counts */
		$counts = [];
		$maxCount = 0;
		
		foreach($votes as $vote)
		{
			$selectedCandidate = $this->getSelectedCandidate($candidates, $vote);
			if(null === $selectedCandidate)
			{
				// if a votes designes a candidate which is not in the list,
				// this is a black vote and it should not count in the results
				continue;
			}
			
			$counted = false;
			
			/** @var FirstPastThePostVoteCount<T> $count */
			foreach($counts as $count)
			{
				if($count->getCandidate()->getId() === $selectedCandidate->getCandidate()->getId())
				{
					$count->increaseCount($selectedCandidate->getPoints());
					$counted = true;
					$maxCount += $selectedCandidate->getPoints();
					break;
				}
			}
			
			if(!$counted)
			{
				$count = new FirstPastThePostVoteCount($selectedCandidate->getCandidate());
				$count->increaseCount($selectedCandidate->getPoints());
				$counts[] = $count;
				$maxCount += $selectedCandidate->getPoints();
			}
		}
		
		// /!\ Memory
		\usort($counts, function(FirstPastThePostVoteCount $count1, FirstPastThePostVoteCount $count2)
		{
			$score = $count2->getCount() - $count1->getCount();
			
			return ((int) (0 < $score)) - ((int) (0 > $score));
		});
		
		/** @var array<integer, CandidateResultInterface<T>> $rankings */
		$rankings = [];
		
		foreach($counts as $count)
		{
			// /!\ Memory
			/** @psalm-suppress InvalidArgument */
			$rankings[] = new CandidateResult(
				$election->getId().'_'.$count->getCandidate()->getId(),
				/** @phpstan-ignore-next-line */
				$count->getCandidate(),
				$count->getCount(),
				new IntegerScore(0, $maxCount, $count->getCount()),
			);
		}
		
		$resultId = $election->getId().'_'.static::class.'_'.\date('Y-m-d H:i:s').'.'.((string) \microtime(true));
		/** @phpstan-ignore-next-line */ /** @psalm-suppress InvalidArgument */
		return new ElectionResult($resultId, $rankings);
	}
	
	/**
	 * Gets the selected candidate from the list of candidates designated by the
	 * vote.
	 * 
	 * @template T of boolean|integer|float|string
	 * @param array<integer, CandidateInterface<T>> $candidates
	 * @param VoteInterface<T> $vote
	 * @return ?CandidateResultInterface<T>
	 */
	public function getSelectedCandidate(array $candidates, VoteInterface $vote) : ?CandidateResultInterface
	{
		$betterCandidate = $this->getBetterCandidate($vote);
		if(null === $betterCandidate)
		{
			// if the vote expresses no rankings, this is a white vote
			// and it should not count in the results
			return null;
		}
		
		/** @var ?CandidateResultInterface<T> $selectedCandidate */
		$selectedCandidate = null;
		
		foreach($candidates as $candidate)
		{
			/** @var CandidateInterface<T> $candidate */
			if($candidate->getId() === $betterCandidate->getCandidate()->getId())
			{
				$selectedCandidate = $betterCandidate;
				break;
			}
		}
		
		return $selectedCandidate;
	}
	
	/**
	 * Gets the better candidate from the vote.
	 * 
	 * @template T of boolean|integer|float|string
	 * @param VoteInterface<T> $vote
	 * @return ?CandidateResultInterface<T>
	 */
	public function getBetterCandidate(VoteInterface $vote) : ?CandidateResultInterface
	{
		$betterCandidate = null;
		$betterScore = 0;
		$candsWithBetterScore = 0;
		
		foreach($vote->getCandidateRanking() as $candidateRanking)
		{
			foreach($candidateRanking->getCandidates() as $candidate)
			{
				$score = (int) ($vote->getOverallScore()->getNormalizedValue() * $candidateRanking->getScore()->getNormalizedValue());
				if(null === $betterCandidate)
				{
					$betterCandidate = $candidate;
					$betterScore = $score;
					$candsWithBetterScore = 1;
				}
				elseif($score > $betterScore)
				{
					$betterCandidate = $candidate;
					$betterScore = $score;
					$candsWithBetterScore = 1;
				}
				elseif($score === $betterScore)
				{
					$candsWithBetterScore++;
				}
			}
		}
		
		if(null === $betterCandidate)
		{
			// no defined candidate. this is a white vote and does not count
			// towards the results
			return null;
		}
		
		if(1 !== $candsWithBetterScore)
		{
			// if the votes has more than one "better" candidate, this is
			// a black vote and it should not count in the results
			return null;
		}
		
		return new CandidateResult($vote->getId(), $betterCandidate, $betterScore, new IntegerScore(0, 1, 1));
	}
	
}
