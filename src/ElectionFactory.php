<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-vote-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Vote;

/**
 * ElectionFactory class file.
 * 
 * This class is a simple implementation of the ElectionFactoryInterface.
 * 
 * @author Anastaszor
 */
class ElectionFactory implements ElectionFactoryInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Vote\ElectionFactoryInterface::createBooleanElection()
	 * @psalm-suppress MoreSpecificReturnType
	 */
	public function createBooleanElection(string $identifier) : ElectionInterface
	{
		/** @phpstan-ignore-next-line */ /** @psalm-suppress LessSpecificReturnStatement */
		return new Election($identifier);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Vote\ElectionFactoryInterface::createIntegerElection()
	 * @psalm-suppress MoreSpecificReturnType
	 */
	public function createIntegerElection(string $identifier) : ElectionInterface
	{
		/** @phpstan-ignore-next-line */ /** @psalm-suppress LessSpecificReturnStatement */
		return new Election($identifier);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Vote\ElectionFactoryInterface::createFloatElection()
	 * @psalm-suppress MoreSpecificReturnType
	 */
	public function createFloatElection(string $identifier) : ElectionInterface
	{
		/** @phpstan-ignore-next-line */ /** @psalm-suppress LessSpecificReturnStatement */
		return new Election($identifier);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Vote\ElectionFactoryInterface::createStringElection()
	 * @psalm-suppress MoreSpecificReturnType
	 */
	public function createStringElection(string $identifier) : ElectionInterface
	{
		/** @phpstan-ignore-next-line */ /** @psalm-suppress LessSpecificReturnStatement */
		return new Election($identifier);
	}
	
}
