<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-vote-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Vote;

/**
 * BooleanCandidate class file.
 * 
 * A candidate that holds boolean values.
 * 
 * @author Anastaszor
 * @extends AbstractCandidate<boolean>
 */
class UniqueBooleanCandidate extends AbstractCandidate
{
	
	/**
	 * The value of this candidate.
	 * 
	 * @var ?boolean
	 */
	protected ?bool $_value;
	
	/**
	 * Builds a new Candidate with its value.
	 * 
	 * @param string $id
	 * @param ?boolean $value
	 */
	public function __construct(string $id, ?bool $value)
	{
		parent::__construct($id);
		$this->_value = $value;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Vote\CandidateInterface::getValue()
	 */
	public function getValue() : ?bool
	{
		return $this->_value;
	}
	
}
