<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-vote-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Vote;

use PhpExtended\Score\ScoreInterface;

/**
 * CandidateResultFactory class file.
 * 
 * This class is a simple implementation of the CandidateResultFactoryInterface.
 * 
 * @author Anastaszor
 */
class CandidateResultFactory implements CandidateResultFactoryInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Vote\CandidateResultFactoryInterface::createCandidateResult()
	 */
	public function createCandidateResult(string $identifier, CandidateInterface $candidate, int $points, ScoreInterface $score) : CandidateResultInterface
	{
		return new CandidateResult($identifier, $candidate, $points, $score);
	}
	
}
