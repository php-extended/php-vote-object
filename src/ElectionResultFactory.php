<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-vote-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Vote;

/**
 * ElectionResultFactory class file.
 * 
 * This class is a simple implementation of the ElectionResultFactoryInterface.
 * 
 * @author Anastaszor
 */
class ElectionResultFactory implements ElectionResultFactoryInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Vote\ElectionResultFactoryInterface::createElectionResult()
	 */
	public function createElectionResult(string $identifier, array $rankings) : ElectionResultInterface
	{
		return new ElectionResult($identifier, $rankings);
	}
	
}
