<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-vote-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Vote;

use RuntimeException;
use Throwable;

/**
 * InvalidVoteException class file.
 * 
 * This class represents the arguments to reject any vote for a specific
 * election.
 * 
 * @author Anastaszor
 * @template T of boolean|integer|float|string
 * @implements InvalidVoteThrowable<T>
 */
class InvalidVoteException extends RuntimeException implements InvalidVoteThrowable
{
	
	/**
	 * The citizen that tried to register the invalid vote.
	 * 
	 * @var CitizenInterface<T>
	 */
	protected CitizenInterface $_citizen;
	
	/**
	 * The invalid vote that tried to be registered.
	 * 
	 * @var VoteInterface<T>
	 */
	protected VoteInterface $_vote;
	
	/**
	 * Builds a new InvalidVoteException with the given.
	 * 
	 * @param CitizenInterface<T> $citizen
	 * @param VoteInterface<T> $vote
	 * @param ?string $message
	 * @param ?integer $code
	 * @param ?Throwable $previous
	 */
	public function __construct(CitizenInterface $citizen, VoteInterface $vote, ?string $message = null, ?int $code = null, ?Throwable $previous = null)
	{
		$this->_citizen = $citizen;
		$this->_vote = $vote;
		
		if(null === $message)
		{
			$message = \strtr('The vote {vote} made by {citizen} is invalid.', [
				'{vote}' => $vote->getId(),
				'{citizen}' => $citizen->getId(),
			]);
		}
		
		if(null === $code)
		{
			$code = -1;
		}
		
		parent::__construct($message, $code, $previous);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Vote\InvalidVoteThrowable::getCitizen()
	 */
	public function getCitizen() : CitizenInterface
	{
		return $this->_citizen;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Vote\InvalidVoteThrowable::getVote()
	 */
	public function getVote() : VoteInterface
	{
		return $this->_vote;
	}
	
}
