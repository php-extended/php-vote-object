<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-vote-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Vote;

use Stringable;

/**
 * FirstPastThePostVoteCount class file.
 * 
 * This class is to count the number of voices of a given candidate.
 * 
 * @author Anastaszor
 * @template T of boolean|integer|float|string
 */
class FirstPastThePostVoteCount implements Stringable
{
	
	/**
	 * The candidate we're counting votes for.
	 * 
	 * @var CandidateInterface<T>
	 */
	protected CandidateInterface $_candidate;
	
	/**
	 * The count of votes for this candidate.
	 * 
	 * @var integer
	 */
	protected int $_count = 0;
	
	/**
	 * Builds a new vote count with the given candidate.
	 * 
	 * @param CandidateInterface<T> $candidate
	 */
	public function __construct(CandidateInterface $candidate)
	{
		$this->_candidate = $candidate;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Gets the candidate for this count.
	 * 
	 * @return CandidateInterface<T>
	 */
	public function getCandidate() : CandidateInterface
	{
		return $this->_candidate;
	}
	
	/**
	 * Increases by one the number of counts.
	 * 
	 * @param integer $value
	 */
	public function increaseCount(int $value = 1) : void
	{
		$this->_count += $value;
	}
	
	/**
	 * Gets the actual count of voices for this candidate.
	 * 
	 * @return integer
	 */
	public function getCount() : int
	{
		return $this->_count;
	}
	
}
