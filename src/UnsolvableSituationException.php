<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-vote-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Vote;

use RuntimeException;
use Throwable;

/**
 * UnsolvableSituationException class file.
 * 
 * This class represents the arguments for when the election process cannot be
 * resolved to an ordered list of candidates for any reason.
 * 
 * @author Anastaszor
 * @template T of boolean|integer|float|string
 * @implements UnsolvableSituationThrowable<T>
 */
class UnsolvableSituationException extends RuntimeException implements UnsolvableSituationThrowable
{
	
	/**
	 * Gets the voting method that failed to order the result set.
	 * 
	 * @var VotingMethodInterface
	 */
	protected VotingMethodInterface $_method;
	
	/**
	 * Gets the candidate result set that cannot be ordered.
	 * 
	 * @var array<integer, CandidateResultInterface<T>>
	 */
	protected array $_results;
	
	/**
	 * Builds a new UnsolvableSituationException with the voting method and
	 * the result set that is derived from the votes and the biases.
	 * 
	 * @param VotingMethodInterface $votingMethod
	 * @param array<integer, CandidateResultInterface<T>> $results
	 * @param ?string $message
	 * @param ?integer $code
	 * @param ?Throwable $previous
	 */
	public function __construct(VotingMethodInterface $votingMethod, array $results, ?string $message = null, ?int $code = null, ?Throwable $previous = null)
	{
		$this->_method = $votingMethod;
		$this->_results = $results;
		
		if(null === $message)
		{
			$message = 'The situation is unresolvable';
		}
		
		if(null === $code)
		{
			$code = -1;
		}
		
		parent::__construct($message, $code, $previous);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Vote\UnsolvableSituationThrowable::getMethod()
	 */
	public function getMethod() : VotingMethodInterface
	{
		return $this->_method;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Vote\UnsolvableSituationThrowable::getResults()
	 */
	public function getResults() : array
	{
		return $this->_results;
	}
	
}
