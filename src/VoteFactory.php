<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-vote-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Vote;

use PhpExtended\Score\ScoreInterface;

/**
 * VoteFactory class file.
 * 
 * This class is a simple implementation of the VoteFactoryInterface.
 * 
 * @author Anastaszor
 */
class VoteFactory implements VoteFactoryInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Vote\VoteFactoryInterface::createVote()
	 */
	public function createVote(string $identifier, array $rankings, ScoreInterface $score) : VoteInterface
	{
		return new Vote($identifier, $score, $rankings);
	}
	
}
