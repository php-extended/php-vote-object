<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-vote-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Vote;

use RuntimeException;
use Throwable;

/**
 * InvalidCandidateException class file.
 * 
 * This class represents the arguments to reject any candidate for a specific
 * election.
 * 
 * @author Anastaszor
 * @template T of boolean|integer|float|string
 * @implements InvalidCandidateThrowable<T>
 */
class InvalidCandidateException extends RuntimeException implements InvalidCandidateThrowable
{
	
	/**
	 * The citizen that tried to register the invalid candidate.
	 * 
	 * @var CitizenInterface<T>
	 */
	protected CitizenInterface $_citizen;
	
	/**
	 * The invalid candidate that tried to be registered.
	 * 
	 * @var CandidateInterface<T>
	 */
	protected CandidateInterface $_candidate;
	
	/**
	 * Builds a new InvalidCandidateException with the given citizen and 
	 * candidate.
	 * 
	 * @param CitizenInterface<T> $citizen
	 * @param CandidateInterface<T> $candidate
	 * @param ?string $message
	 * @param ?integer $code
	 * @param ?Throwable $previous
	 */
	public function __construct(CitizenInterface $citizen, CandidateInterface $candidate, ?string $message = null, ?int $code = null, ?Throwable $previous = null)
	{
		$this->_citizen = $citizen;
		$this->_candidate = $candidate;
		
		if(null === $message)
		{
			$message = \strtr('The candidate {vote} given by {citizen} is invalid.', [
				'{candidate}' => $candidate->getId(),
				'{citizen}' => $citizen->getId(),
			]);
		}
		
		if(null === $code)
		{
			$code = -1;
		}
		
		parent::__construct($message, $code, $previous);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Vote\InvalidCandidateThrowable::getCitizen()
	 */
	public function getCitizen() : CitizenInterface
	{
		return $this->_citizen;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Vote\InvalidCandidateThrowable::getCandidate()
	 */
	public function getCandidate() : CandidateInterface
	{
		return $this->_candidate;
	}
	
}
