<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-vote-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Vote;

use PhpExtended\Score\ScoreInterface;

/**
 * CandidateRankingFactory class file.
 * 
 * This class is a simple implementation of the CandidateRankingFactoryInterface.
 * 
 * @author Anastaszor
 */
class CandidateRankingFactory implements CandidateRankingFactoryInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Vote\CandidateRankingFactoryInterface::createCandidateRanking()
	 */
	public function createCandidateRanking(string $identifier, array $candidates, ScoreInterface $score) : CandidateRankingInterface
	{
		return new CandidateRanking($identifier, $score, $candidates);
	}
	
}
