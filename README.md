# php-extended/php-vote-object
A library that implements the php-vote-object package

![coverage](https://gitlab.com/php-extended/php-vote-object/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-vote-object/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-vote-object ^8`


## Basic Usage :

To run this library, you need at least one other library, which is to provide
a specific voting method implementing the 
`PhpExtended\Vote\VotingMethodInterface` interface.

Additionnally, an additional library that provides the biases to modify the
votes before the election is runned may be used. The biases must implement the
`PhpExtended\Vote\BiasInterface` interface.

Finally, the core of the voting system is to vote for something, so you must
provide a library which creates citizens for the voting process, such citizens
may vote and rank any candidate in the election. All the citizens must implement
the `PhpExtended\Vote\CitizenInterface` interface.
Those citizens are very important, they provide the candidates and their
arguments. Their arguments is what will be examined by the different citizens
when they will be choosing for a vote ordering, so the citizen methods must be 
coherent!

Once every piece comes together, the engine may be used the following way :

```php

use PhpExtended\Vote\ElectionRunner;
use PhpExtended\Vote\InvalidCandidateException;
use PhpExtended\Vote\InvalidVoteException;
use PhpExtended\Vote\UnsolvableSituationException;

$runner = new ElectionRunner();
$biases = new ArrayIterator(array(
	$yourBiasedObjects,	// each implement BiasInterface
));
$citizens = new ArrayIterator(array(
	$yourCitizenObjects,	// each implement CitizenInterface
));
$method = $theChoosenMethod;	// implements VotingMethodInterface

try
{
	$result = $runner->runElection($method, $biases, $citizens);
}
catch(InvalidCandidateException $ice)
{
	// TODO handle the fact that one candidate is refused
}
catch(InvalidVoteException $ive)
{
	// TODO handle the fact that one vote is refused
}
catch(UnsolvableSituationException $use)
{
	// TODO handle the fact that an ordering between the 
	// candidates could not be decided by the current voting
	// method and the votes that were given.
}

foreach($result->getRankings() as $individualResult)
{
	// The rankings are given in order of importance, winner
	// first and loser last
	/* @var $individualResult \PhpExtended\Vote\ElectionResultInterface */
	$candidate = $individualResult->getCandidate();
	// here the candidate is one of the candidate object that
	// was given by one of your citizens, and you retrieve
	// its objects within the arguments of the citizen.
}

```


This library proposes a list of standard citizens that will vote with a 
predetermined behavior. They are useful to use in an election the following
way :

```php

use PhpExtended\Vote\ElectionRunner;
use PhpExtended\Vote\InvalidCandidateException;
use PhpExtended\Vote\InvalidVoteException;
use PhpExtended\Vote\UnsolvableSituationException;
use PhpExtended\Vote\UniqueStringEqualsCitizen;

$runner = new ElectionRunner();
$citizens = array();
foreach($yourObjectsThatProvideStrings as $yourObject)
{
	$id = $yourObject->__toString();	// or anything else that may be useful for an id
	$citizens[] = new UniqueStringEqualsCitizen($id, $yourObject->yourMethodThatReturnsString());
}

try
{
	$runner->runElection($method, new ArrayIterator(array()), new ArrayIterator($citizens)); 
}
catch(InvalidCandidateException $ice)
{
	// TODO handle the fact that one candidate is refused
}
catch(InvalidVoteException $ive)
{
	// TODO handle the fact that one vote is refused
}
catch(UnsolvableSituationException $use)
{
	// TODO handle the fact that an ordering between the 
	// candidates could not be decided by the current voting
	// method and the votes that were given.
}

```


### UniqueBooleanEqualsCitizen

This citizen proposes a candidate with only one argument which is a boolean
value. This citizen ranks all the candidates with 100% if they have only one
argument which is the boolean value, and 0% for anyone else.


### UniqueFloatEqualsCitizen

This citizen proposes a candidate with only one argument which is a float
value. This citizen ranks all the candidates with 100% if they have only one
argument which is the float value, and 0% for anyone else.


### UniqueIntegerEqualsCitizen

This citizen proposes a candidate with only one argument which is an integer
value. This citizen ranks all the candidates with 100% if they have only one
argument which is the integer value, and 0% for anyone else.


### UniqueStringEqualsCitizen

This citizen proposes a candidate with only one argument which is a string
value. This citizen ranks all the candidates with 100% if they have only one
argument which is the string value, and 0% for anyone else.


## License

MIT (See [license file](LICENSE)).
